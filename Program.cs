﻿using VidyakinLibrary;
namespace ConsoleApp1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            DBController controller = new DBController();
            AuthHandler authHandler = new AuthHandler();

            Console.WriteLine(authHandler.createNewUser(controller, "Vasya", "Pupkin", "4532444555", 0, "1234"));

            Console.WriteLine(authHandler.checkUserData(controller, "Vasya", "Pupkin", "4532444555", 0, "123"));
            Console.WriteLine(authHandler.checkUserData(controller, "Vasya", "Pupkin", "4532444556", 0, "1234"));
            Console.WriteLine(authHandler.checkUserData(controller, "Vasya", "Pupkin", "4532444555", 0, "1234"));

            ProjectsHandler handler = new ProjectsHandler(controller);

            Console.WriteLine(handler.addProject(controller, "IT", "Game", "Cool game", authHandler.user));
            Console.WriteLine(handler.addProject(controller, "Management", "1C", "Classic", authHandler.user));

            Console.WriteLine(handler.editVerdict(controller, "Game", true, true, true, true, "Send to fund holder"));

            Console.WriteLine(authHandler.createNewUser(controller, "Sasha", "Bely", "4519228322", 0, "haha"));

            Console.WriteLine(handler.addProject(controller, "IT", "Notepad", "Plusplus", authHandler.user));

            Console.WriteLine(authHandler.checkUserData(controller, "Vasya", "Pupkin", "4532444555", 0, "1234"));

            List<List<string>> testlist = authHandler.user.getAllRequests(handler);

            Console.WriteLine(handler.editVerdict(controller, "Game", "Approved"));
        }
    }
}