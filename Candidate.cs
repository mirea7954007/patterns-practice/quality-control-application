﻿namespace VidyakinLibrary
{
    public class Candidate : IUser
    {
        protected string name;
        protected string surname;
        protected string passport;
        public Candidate(string name, string surname, string passport)
        {
            this.name = name;
            this.surname = surname;
            this.passport = passport;
        }

        public List<List<string>> getAllRequests(ProjectsHandler projectsHandler)
        {
            List<Verdict> listForExpert = projectsHandler.getListForCandidate(this.passport);

            List<List<string>> resultVerdicts = new List<List<string>>();

            foreach (Verdict verdict in listForExpert)
            {
                resultVerdicts.Add(adaptVerdict(verdict));
            }
            return resultVerdicts;
        }
        public List<string> getCredits()
        {
            return new List<string>() { this.name, this.surname, this.passport };
        }

        public List<string> adaptVerdict(Verdict verdict)
        {
            List<bool> grades = verdict.getGrades();

            List<string> resultVerdict = new List<string>() { grades[0].ToString(), grades[1].ToString(), grades[2].ToString(), grades[3].ToString() };

            resultVerdict.Add(verdict.getRecomendation());
            resultVerdict.AddRange(verdict.getRequestData());

            return resultVerdict;
        }
    }
}
