﻿using Microsoft.Data.Sqlite;

namespace VidyakinLibrary
{
    public class ProjectsHandler
    {
        private List<Verdict> verlist;
        
        private void reloadList(List<Verdict> templist)
        {
            verlist.Clear();
            verlist.AddRange(templist);
        }
        public ProjectsHandler(DBController dbcontroller)
        {
            verlist = new List<Verdict>();
            reloadList(dbcontroller.getProjects());
        }

        public string addProject(DBController dbcontroller, string focus, string name, string description, IUser user)
        {
            try
            {
                string result = dbcontroller.addProject(focus, name, description, user);
                if (result == "Successfull project registration")
                {
                    reloadList(dbcontroller.getProjects());
                    return "Request has been registered";
                }
                else
                {
                    throw new Exception(result);
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public string editVerdict(DBController dBController, string name, string recomendation)
        {
            try
            {
                string result = dBController.editProject(name, recomendation);
                if (result == "Successfull evaluation")
                {
                    reloadList(dBController.getProjects());
                    return "Project has been evaluated";

                }
                else
                {
                    throw new Exception(result);
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public string editVerdict(DBController dBController, string name, bool grade1, bool grade2, bool grade3, bool grade4, string recomendation)
        {
            try
            {
                string result = dBController.editProject(name, grade1, grade2, grade3 , grade4, recomendation);
                if (result == "Successfull evaluation")
                {
                    reloadList(dBController.getProjects());
                    return "Project has been evaluated";

                }
                else
                {
                    throw new Exception(result);
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public List<Verdict> getListForCandidate(string passport)
        {
            List<Verdict> templist = new List<Verdict>();
            foreach(Verdict ver in verlist)
            {
                if (ver.getRequestData()[5]==passport)
                    templist.Add(ver);
            }
            return templist;
        }
        public List<Verdict> getListForExpert()
        {
          List<Verdict> templist = new List<Verdict>();
          foreach (Verdict ver in verlist)
          {
            if (ver.getRecomendation() == "")
              templist.Add(ver);
          }
          return templist;
        }
        public List<Verdict> getListForFundholder()
        {
          List<Verdict> templist = new List<Verdict>();
          foreach (Verdict ver in verlist)
          {
            if (ver.getRecomendation() == "Send to fund holder")
              templist.Add(ver);
          }
          return templist;
        }
  }
}
